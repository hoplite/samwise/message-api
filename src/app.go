package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
)

const MsgRetries = 50

type App struct {
	Broker BrokerClient
	Router *mux.Router
}

func (a *App) Initialize() {
	log.Println("Starting service..")
	a.Router = mux.NewRouter()
	bc, err := SetupClient()
	a.Broker = bc
	if err != nil {
		failOnError(err, "Could not setup broker!")
	}
	a.InitializeRoutes()
}

// Run starts the app and serves on the specified addr
func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, a.Router))
}

func (a *App) InitializeRoutes() {
	a.Router.HandleFunc("/api/v1/messages/", a.ReceiveMessage).Methods("POST")
	a.Router.HandleFunc("/health", Health).Methods("GET")
}

func Health(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("OK"))
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func SetupClient() (bc BrokerClient, err error) {
	// Default to AMQP
	clientType := os.Getenv("MSG_API_BROKER")
	log.Println("Setting broker type", clientType)
	switch clientType {
	case "AMQP":
		ac := &AmqpClient{}
		bc = ac

	default:
		err = fmt.Errorf(
			"JSON error: Could not find a client for type %s", clientType)
	}
	return bc, err

}

func (a *App) ReceiveMessage(w http.ResponseWriter, r *http.Request) {
	message := NewMessage()

	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		log.Printf("Error: %s", err)
	}
	if err := r.Body.Close(); err != nil {
		log.Printf("Error: %s", err)
	}
	if err := json.Unmarshal(body, &message); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessablle entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Printf("JSON Encode Error: %s", err)
		}
	}
	err = message.Validate()
	if err != nil {
		log.Printf("Validate err %s", err)

		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422)
		w.Write([]byte(err.Error()))
		log.Println("Message encoding error", err)
		return
	}
	log.Printf(
		"Id: %s Service: %s RoomId %s",
		message.Id,
		message.Service,
		message.RoomId)
	log.Println("Message Text:", message.MessageText)
	log.Println("Markdown Text:", message.MarkdownText)
	err = a.Broker.SendMessage(message)
	if err != nil {
		go MsgRetry(MsgRetries, time.Second, func() error {
			err = a.Broker.SendMessage(message)
			if err != nil {
				return err
			}
			log.Println("Message sent")
			return nil
		})
	}
	w.Write([]byte(message.Id))
}
