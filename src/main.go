package main

import (
	"os"
)

func main() {
	appPort := ":" + os.Getenv("APP_PORT")
	a := App{}
	a.Initialize()
	a.Run(appPort)
}
