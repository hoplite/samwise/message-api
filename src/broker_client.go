package main

import (
	"encoding/json"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/streadway/amqp"
)

type BrokerClient interface {
	SendMessage(*Message) error
}

// Perform recursive retries until don't have anymore
// Inspired by this blog https://upgear.io/blog/simple-golang-retry-function/
func MsgRetry(attempts int, sleep time.Duration, f func() error) error {
	if err := f(); err != nil {
		if attempts--; attempts > 0 {
			log.Printf("Retrying attempt %v", attempts)
			// Add some randomness to prevent creating a Thundering Herd
			jitter := time.Duration(rand.Int63n(int64(sleep)))
			sleep = sleep + jitter/2

			time.Sleep(sleep)
			return MsgRetry(attempts, 2*sleep, f)
		}
		log.Printf("Failed after max retries")
		return err
	}

	return nil
}

type AmqpClient struct {
	BrokerClient
}

func (ac AmqpClient) SendMessage(message *Message) error {
	conn, err := amqp.Dial(os.Getenv("AMQP_URL"))
	if err != nil {
		log.Printf("Failed to connect to RabbitMQ. %s", err)
		return err
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Printf("Failed to open a channel. %s", err)
		return err
	}
	defer ch.Close()

	err = ch.ExchangeDeclare(
		message.Service, // name
		"fanout",        // type
		false,           // durable
		false,           // auto-deleted
		false,           // internal
		false,           // noWait
		nil,             // arguments
	)
	if err != nil {
		log.Printf("Failed to declare a queue. %s", err)
		return err
	}

	body, err := json.Marshal(message)
	if err != nil {
		log.Printf("Failed to marshal message to json. %s", err)
		return err
	}

	err = ch.Publish(
		message.Service, // exchange
		"",              // routing key
		false,           // mandatory
		false,           // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        body,
		})
	if err != nil {
		log.Printf("Failed to publish message. Retrying... %s", err)
		return err
	}
	return nil
}
